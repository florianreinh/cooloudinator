import React from 'react';
import ReactDOM from 'react-dom';
import './semantic/dist/semantic.min.css';
import {Button, Container, Header, Modal} from 'semantic-ui-react'

import moment from 'moment'
import 'moment/locale/de'
import {createStore} from "redux";

import mainReducer from './reducers/main_reducer'
import updateHistoData from './actions/actions'

const store = createStore(mainReducer);


class WeatherRows extends React.Component {
    componentDidMount() {
        const today = new Date();
        const url = `https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/638242/${today.getFullYear()}/${today.getMonth() + 1}/${today.getDate()}/`;

        store.subscribe(() => this.setState(store.getState()));

        fetch(url, {mode: 'cors'})
            .then(res => res.json())
            .then((berlinWeatherHistoData) => {
                store.dispatch(updateHistoData({historyData: berlinWeatherHistoData}))
            });
    }

    render() {
        return store.getState().historyData.map((entry, i) => <Row key={entry.id} histo={Object.assign(entry, {rowIndex: i})}/>);
    }
}

function Row(props){
    const histo = props.histo;
    let created = moment(histo.created);

    created.locale('de');

    return (
        <Container style={{'borderTop': '1px solid', 'marginTop': '10px', 'paddingTop': '10px'}}>
            <Header>
                Berlin {created.format('lll')}
            </Header>
            <div className={'ui grid'}>
                <div className={'temp-min three wide column'}>Temp(min): {histo.min_temp}</div>
                <div className={'temp-max three wide column'}>Temp(max): {histo.max_temp}</div>
                <div className={'temp-current three wide column'}>Temp(curr): {histo.the_temp}</div>
            </div>

            <Modal trigger={<Button primary>More Info</Button>}>
                <Modal.Header>More Info</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <Header>Weather Details</Header>
                        <div className={'ui grid'}>
                            <div className={'weather-state four wide column'}>Weather state: {histo.weather_state_name} {histo.weather_state_abbr}</div>
                            <div className={'wind-state four wide column'}>Wind state: {histo.wind_direction_compass}</div>
                            <div className={'wind-speed four wide column'}>Wind speed: {histo.wind_speed}</div>
                            <div className={'wind-direction four wide column'}>Wind direction: {histo.wind_direction}</div>
                        </div>
                        <div className={'ui grid'}>
                            <div className={'humidity four wide column'}>Humidity: {histo.humidity}%</div>
                            <div className={'air-pressure four wide column'}>Air pressure: {histo.air_pressure}</div>
                            <div className={'visibility four wide column'}>Visibility: {histo.visibility}</div>
                            <div className={'predictability four wide column'}>Predictability: {histo.predictability}%</div>
                        </div>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        </Container>
    );
}

class WeatherBoard extends React.Component {
    render() {
        return (
            <div className="weatherboard">
                <WeatherRows />
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <WeatherBoard />,
    document.getElementById('root')
);
