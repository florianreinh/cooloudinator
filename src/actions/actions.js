export default function updateHistoData(newData){
    return {type: 'UPDATE', data: newData};
}
