const initialState = { historyData: [] };

function mainReducer(state = initialState, action){
    if (action.type === 'UPDATE') {
        let newState = Object.assign({}, state, {historyData: state.historyData.concat(action.data.historyData)});
        return newState;
    }

    return state;
}

export default mainReducer;
